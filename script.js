//Теоретичне питання
//Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
//це технологія, що дозволяє взаємодіяти з веб-сторінкою без необхідності перезавантажувати її
//В основі лежить використання JavaScript для асинхронного відправлення запитів
//на сервер та отримання відповіді без перезавантаження сторінки

//Завдання
//Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

//Технічні вимоги:
//Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films
//та отримати список усіх фільмів серії Зоряні війни
//Для кожного фільму отримати з сервера список персонажів,
//які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
//Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані.
//Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
//Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран
//під назвою фільму.
//Необов'язкове завдання підвищеної складності
//Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження.
//Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

// Код починається з оголошення API, яка містить URL-адресу з якої будуть отримуватися дані про фільми.
// Також оголошується змінна root, яка вказує на DOM-елемент з id="root", в який будуть вставлятися результати запитів.

const API = "https://ajax.test-danit.com/api/swapi/films";
const root = document.querySelector("#root");
const charactersArr = [];

// Функція renderFilms() створює список фільмів, використовуючи отриманий масив фільмів.
// Для кожного фільму в масиві створюється елемент списку з інформацією про episodeId, name, openingCrawl, characters
// Для кожного фільму також створюється порожній масив charactersArr, в який пізніше будуть додані URL-адреси персонажів,
// що з'являються у фільмі.

const renderFilms = (films) => {
  const filmsList = document.createElement("ul");
  filmsList.classList.add("films");

  films.forEach(({ episodeId, name, openingCrawl, characters }) => {
    charactersArr.push(characters);
    filmsList.innerHTML += `
			<li class="films__card">
				<h2 class="films__name">${name}</h2>
				<p class="films__episode">Episode: ${episodeId}</p>
				<p class="films__descr">
					${openingCrawl}
				</p>
				<div class="actors">
					<div class="lds-spinner spinner">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
				</div>
			</li>
		`;
  });

  getCharacters(charactersArr);

  root.append(filmsList);
};

// Якщо запити виконалися успішно, то викликається функція renderCharacters, яка отримує масив даних про charactersArr масив
//  елементів HTML, куди потрібно вставити список actors та index
//  Для кожного елементу actors[index] додається заголовок <h2> та елемент списку <ul> до якого додаються дані про акторів

const renderCharacters = (charactersArr, actors, index) => {
  const actorsList = document.createElement("ul");
  actorsList.classList.add("actors__list");

  charactersArr.forEach(({ name }) => {
    actorsList.innerHTML += `
						<li class="actors__name">${name}</li>
					`;
  });

  actors[index].innerHTML = `<h2 class="actors__title">Actors:</h2>`;
  actors[index].append(actorsList);
};

// Функція getFilms() використовує fetch() для отримання даних про фільми з API за допомогою вказаної адреси API.
// Після успішного отримання відповіді вона викликає функцію renderFilms(), передаючи в якості аргументу отриманий масив фільмів.
//  У випадку помилки вона виводить повідомлення про помилку.

const getFilms = (url) => {
  fetch(url)
    .then((response) => response.json())
    .then(renderFilms)
    .catch((err) => alert(err.message));
};

// Функція getCharacters() отримує масив charactersArr, який містить URL-адреси персонажів, що з'являються у фільмах,
// і для кожного фільму запускає послідовність запитів на сервер, щоб отримати дані про кожного персонажа.
//  Функція використовує Promise.all() для запуску послідовності запитів, що повертає масив результатів запитів на сервер.

const getCharacters = (charactersArr) => {
  charactersArr.forEach((characters, index) => {
    const promiceArr = [];

    characters.forEach((characterUrl) => {
      promiceArr.push(fetch(characterUrl).then((response) => response.json()));
    });

    Promise.all(promiceArr)
      .then((charactersArr) => {
        const actors = document.querySelectorAll(".actors");
        renderCharacters(charactersArr, actors, index);
      })
      .catch((err) => alert(err.message));
  });
};

getFilms(API);
